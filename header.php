<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package byte
 */

?>
<!doctype html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#42003e">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'byte' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="main-menu">
			<nav id="site-navigation-left" class="main-navigation left">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-left',
						'menu_id'        => 'primary-menu-left',
					)
				);
				?>
			</nav><!-- #site-navigation -->

			<div class="site-branding logo">
				<?php the_custom_logo(); ?>
			</div><!-- .site-branding -->
			
			<nav id="site-navigation-right" class="main-navigation right">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-right',
						'menu_id'        => 'primary-menu-right',
					)
				);
				?>
			</nav><!-- #site-navigation -->
		</div>
		<div class="mobile-menu">
			<div class="menu-bar">
				<div class="menu-icon-box">
					<a class="menu-icon"><i class="fas fa-bars"></i><a>
					<a class="menu-icon-close"><i class="fas fa-times"></i><a>
				</div>
				<div class="mobile-logo">
					<?php the_custom_logo(); ?>
				</div>
			</div>
			<div class="menu-list">
				<nav class="mobile-navigation">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'mobile-menu',
							'menu_id'        => 'mobile-menu-nav',
						)
					);
					?>
				</nav><!-- .mobile-navigation -->
			</div>
		</div>
	</header><!-- #masthead -->
