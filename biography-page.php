<?php
/**
 * Biography page template file
 * Template Name: Biography Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

get_header();
?>

	<main id="primary" class="biography-page site-main">
        <section class="section-1">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name'  => 'biography',
				'ACF_Name'   => 'biography_page_header_slider',
				'title'      => true,
				'subtitle'   => true,
				'slider'     => true,
				'classes'    => array(
					'title'      => 'title3-style',
					'subtitle'   => 'subtitle2-style',
					'slider'     => 'biographyPage-header-slider',
					'titleBox'   => 'title-box-2',
					'boxClass'   => 'absolute-box',
				)
			));
			?>
		</section><!-- .section-1 -->

		<section class="section-2">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name'  => 'biography',
				'ACF_Name'   => 'biography_page_section_2',
				'title'      => true,
				'subtitle'   => true,
				'content'    => true,
				'classes'    => array(
					'title'      => 'title-style',
					'subtitle'   => 'subtitle-style',
					'content'    => 'content-style',
				)
			));
			?>
		</section><!-- .section-2 -->

		<section class="sections section-3">
			<?php
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name'  => 'biography',
				'ACF_Name'   => 'biography_page_section_3',
				'title'      => true,
				'content'    => true,
				'skills'     => true,
				'classes'    => array(
					'title'      => 'title-style',
					'content'    => 'content-style',
				)
			));
			?>
		</section><!-- .section-3 -->

		<section class="sections section-4">
			<?php
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name'  => 'biography',
				'ACF_Name'   => 'biography_page_section_4',
				'title'      => true,
				'degrees'     => true,
				'classes'    => array(
					'title'      => 'title-style',
				)
			));
			?>
		</section><!-- .section-4 -->

		<section class="sections section-5">
			<?php
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name'  => 'biography',
				'ACF_Name'   => 'biography_page_section_5',
				'title'      => true,
				'experiences'     => true,
				'classes'    => array(
					'title'      => 'title-style',
				)
			));
			?>
		</section><!-- .section-5 -->

		<section id="contact-me" class="sections section-6">
			<?php
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name'  => 'biography',
				'ACF_Name'   => 'biography_page_section_6',
				'title'      => true,
				'social_media'      => true,
				'experiences'     => true,
				'classes'    => array(
					'title'      => 'title-style',
				)
			));
			?>
		</section><!-- .section-6 -->
	</main><!-- .biography-page -->

<?php
get_footer();
