<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

get_header();
?>

	<main id="primary" class="site-main archive-blog-page">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				
				$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'full'); 
				$featured_image = $img[0];
				?>
				<img class="blog-img" src="<?php echo $featured_image; ?>" alt="">
				<section class="sections">
					<h1 class="title-style"><?php single_post_title(); ?></h1>
					<?php $page_for_posts_id = get_option( 'page_for_posts' ); ?>
					<div class="content-style">
					<?php echo apply_filters( 'the_content', get_post_field( 'post_content', $page_for_posts_id ) ); ?>
					</div>
				</section>
			
				<?php
			endif;
			?>
			<section class="sections articles">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-archive', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</section>
	</main><!-- #main -->

<?php
get_footer();
