<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

get_header();
?>

<main id="primary" class="site-main archive-cat-page">

    <?php if ( have_posts() ) { ?>
    <?php 
			
			$terms = get_queried_object();
			$image = get_field('project_categories_thumbnail', $terms);
			if($image){
				echo '<img class="blog-img" src="' . esc_url($image['url'] ) . '" alt="' . esc_attr( $image['alt'] ) . '"/>';
			}
			$terms = get_terms( $terms->taxonomy, array(
				'parent'    => $terms->term_id,
				'hide_empty' => false
			));
			if ( $terms && !is_wp_error( $terms ) ) {
				$count = 0;
				?>
    <section class="sections">
        <?php the_archive_title( '<h1 class="title-style-archive">', '</h1>' );
					$myterm = get_queried_object();
					$subtitle = get_field('project_categories_subtitle', $myterm);
					if($subtitle){ ?>
        <span class="subtitle-style-archive"><?php echo esc_html($subtitle); ?></span>
        <?php } ?>
        <?php the_archive_description( '<div class="content-style">', '</div>' );?>
    </section><!-- .sections -->

    <section class="articles">
        <?php
				/* Start the Loop */
				foreach ( $terms as $term ) {
					$count++;
					$term_link = get_term_link( $term );
					echo '<article class="items wow bounceInUp '. ($count % 2 == 0 ? 'even' : '') .'">';
					echo '<div class="left">';
					echo '<div class="title-box">';
					echo '<a href="' . $term_link . '"><h2 class="title2-style">' . strtoupper($term->name) . '</h2></a>';
					$subtitle = get_field('project_categories_subtitle', $term);
					if ($subtitle) {
						echo '<h3 class="subtitle-style">' . esc_html(strtoupper($subtitle)) . '</h3>';      
					}
					echo '</div>';
					echo '<p class="content2-style">' . $term->description . '</p>';        
					echo '</div>';
					echo '<div class="right">';
					echo '<div class="archive-cat-slider">';

					$args = array('post_type' => 'projects','numberposts' => '4', 'tax_query' => array(
						array(
							'taxonomy' => $term->taxonomy,
							'field' => 'term_id',
							'terms'    => $term->term_id,
							'operator' => 'IN'
						)
					) );
					$recent_posts = wp_get_recent_posts( $args );

					$tmp = 0;
					foreach( $recent_posts as $recent ){
						if($tmp>4){
							break;
						}
						$tmp++;
						the_post();
						echo '<a class="" href="' . $term_link . '">' . get_the_post_thumbnail( $recent["ID"] ) . '</a>';
					}
					echo '</div>';
					echo '</div>';
					echo '</article>';
				
				}
			}else{
				?>
        <section class="sections single-cat-archive">
            <div class="inner">
                <div class="title-box">
                    <?php
							the_archive_title( '<h1 class="title2-style">', '</h1>' );
							$terms = get_queried_object();
							$subtitle = get_field('project_categories_subtitle', $terms);
							if ($subtitle) {
								echo '<h3 class="subtitle-style">' . esc_html(strtoupper($subtitle)) . '</h3>';      
							}
							?>
                </div>
                <?php the_archive_description( '<div class="content2-style">', '</div>' ); ?>
            </div>
            <?php
					while ( have_posts() ) {
						the_post();
						echo '<div class="single-cat-posts-box">';
						echo '<div class="single-cat-posts-slider">';
						echo '<a data-lightbox="cat-img-'. get_the_id() .'" data-title="'. get_the_title() .'<br><p>'. get_the_content() .'</p>" href="' . get_the_post_thumbnail_url() . '">' . get_the_post_thumbnail() . '</a>';
						$sliders = get_field('projects_slider');
						if ($sliders){
							foreach($sliders as $slider){
								if($slider){
									echo '<a data-lightbox="cat-img-'. get_the_id() .'" data-title="'. get_the_title() .'<br><p>'. get_the_content() .'</p>" href="' . esc_url($slider['url']) . '"><img src="' . esc_url($slider['url']) . '" alt="' . esc_html($slider['alt']) . '"></a>';
								}
							}
						}
						echo '</div>';
						echo '<div class="inner">';
						the_title('<h2 class="single-cat-title-style">', '</h2>');
						$location = get_field('projects_location');
						if ($location) {
							echo '<span>'. esc_html($location) .'</span>';
						}
						$date = the_date( '', '<span>', '</span>');
						if ($date){
							$date;
						}
						echo '</div>';
						echo '</div>';
					}
					?>
        </section><!-- .sections -->

        <?php } ?>
    </section>
    <?php }?>
</main><!-- #main -->
<?php
get_footer();