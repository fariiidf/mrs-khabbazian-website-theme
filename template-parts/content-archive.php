<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

?>
<article class="archive-article" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php byte_post_thumbnail(); ?>

    <div class="inner">
        <?php
        if ( is_singular() ) :
            the_title( '<h1 class="entry-title">', '</h1>' );
        else :
            the_title( '<h2 class="archive-title">', '</h2>' );
        endif;
        if ( 'post' === get_post_type() ) :
            ?>
        <?php 
        endif; 
        
        $excerpt = get_the_excerpt(); 
        $excerpt = substr( $excerpt, 0, 150 );
        $result = substr( $excerpt, 0, strrpos( $excerpt, ' ' ) );
        echo '<p class="archive-excerpt">' . $result . '</p>';
        ?>
        
        <a class="read-more2" href="<?php echo esc_url(get_permalink()); ?>">READ MORE</a>
    </div>
    
</article><!-- #post-<?php the_ID(); ?> -->
