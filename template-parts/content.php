<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php the_post_thumbnail('full', array('class' => 'blog-img')); ?>
	<div class="sections">
		<?php the_title( '<h1 class="title-style">', '</h1>' ); ?>
		<div class="content-style">
			<?php the_content(); ?>
		</div>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->