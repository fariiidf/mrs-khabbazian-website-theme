<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

?>
<?php 

foreach ( $terms as $term ) {
    $term_link = get_term_link( $term );
    echo '<div class="items wow bounceInUp">';
    echo '<div class="title-box">';
    echo '<h2 class="title2-style">' . strtoupper($term->name) . '</h2>';      
    echo '<h3 class="subtitle-style">' . strtoupper($term->parent_name) . '</h3>';      
    echo '</div>';
    echo '<p class="content2-style">' . strtoupper($term->description) . '</p>';        
    echo '<div class="archive-cat-slider">';
    while ( have_posts() ) :
        the_post();

        byte_post_thumbnail();
        
    endwhile;
    echo '</div>';
    echo '</div>';
}

