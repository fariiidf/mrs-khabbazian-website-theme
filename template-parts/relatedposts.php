<?php
/**
 * Template part for displaying related posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */
?>

<?php
$related_query = new WP_Query(array(
    'post_type' => 'post',
    'category__in' => wp_get_post_categories(get_the_ID()),
    'post__not_in' => array(get_the_ID()),
    'posts_per_page' => 3,
    'orderby' => 'date',
));
?>
<?php if ($related_query->have_posts()) { ?>

<section class="sections">

    <h2 class="title-style">Related Researches</h2>

    <div class="related-posts">

        <?php while ($related_query->have_posts()) { ?>

            <?php $related_query->the_post(); ?>

            <?php 

                $excerpt = get_the_excerpt(); 
                $excerpt = substr( $excerpt, 0, 150 );
                $result = substr( $excerpt, 0, strrpos( $excerpt, ' ' ) );
                echo '<div class="items wow bounceInUp">';
                echo '<a class="img" href="' . esc_url(get_permalink()) . '">';
                the_post_thumbnail();
                echo '</a>';
                echo '<div class="inner">';
                the_title('<h3 class="archive-title">','</h3>');
                echo '<p class="archive-excerpt">' . $result . '</p>';
                echo '<a class="read-more2" href="' . esc_url(get_permalink()) . '">READ MORE</a>';
                echo '</div>';
                echo '</div>';

            ?>

        <?php } ?>

    </div>
    
</section>

<?php wp_reset_postdata(); ?>

<?php } ?>