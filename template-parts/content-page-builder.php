<?php
/**
 * Template part for displaying content of pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

?>
<?php
if ($args['page_name']) {
    $pageName = $args['page_name'];
}
if ($args['has_postType'] == true) {
    $hasPostType = $args['has_postType'];
}
if ($args['has_post'] == true) {
    $hasPost = $args['has_post'];
}
if ($args['classes']){
    $class = $args['classes'];
    if($class['title']){
        $titleClass = $class['title'];
    }
    if($class['subtitle']){
        $subtitleClass = $class['subtitle'];
    }
    if($class['content']){
        $contentClass = $class['content'];
    }
    if($class['button']){
        $buttonClass = $class['button'];
    }
    if($class['slider']){
        $sliderClass = $class['slider'];
    }
    if ($class['titleBox']) {
        $titleBox = $class['titleBox'];
    }
    if ($class['boxClass']) {
        $boxClass = $class['boxClass'];
    }
}
if ($args['ACF_Name']) {
    // Display Contents From ACF Plugin
    $main = get_field($args['ACF_Name']); // ACF FUNCTION
	if ($args['title'] == true ){
        $title = $main['title'];
    }    
    if ($args['subtitle'] == true ){
        $subtitle = $main['subtitle'];
    }
    if ($args['content'] == true ){
        $content = $main['content'];
    }   
    if ($args['link'] == true ){
        $link = $main['link'];
    }
    if ($args['image'] == true ){
        $image = $main['image'];
    }
    if ($args['slider'] == true ){
        $sliders = $main['slider'];
    }
    if ($args['skills'] == true ){
        $skills = $main['skills'];
    }
    if ($args['degrees'] == true ){
        $degrees = $main['degrees'];
    }
    if ($args['experiences'] == true ){
        $experiences = $main['experiences'];
    }
    if ($args['social_media'] == true ){
        $socialMedia = $main['social_media_links'];
        $subtitle1 = $main['subtitle_1'];
        $subtitle2 = $main['subtitle_2'];
        $contactForm = $main['contact_form'];
    }

    if ($pageName) {
        // Home Page
        if ($pageName == 'home') {
            echo '<div class="box">';
            // Display Content Boxes
            if(isset($title, $content, $link)){
                echo '<div class="inner wow bounceInUp">';
                echo '<h2 class="' . ($titleClass ? $titleClass : '') . '">' . esc_html($title) . '</h2>';
                echo '<p class="' . ($contentClass ? $contentClass : '') . '">' . esc_html($content) . '</p>';
                echo '<a class="' . ($buttonClass ? $buttonClass : '') .'" href="' . esc_url($link) . '">READ MORE</a>';
                echo '</div>';
                echo '</div>';
            }
            elseif(isset($title, $subtitle, $content)){
                if($hasPostType){
                    $firstClass = 'right homePage-sec6-slider';
                    $secondClass = 'left';
                }else {
                    $firstClass = 'left';
                    $secondClass = 'right';
                }
                echo '<div class="' . $firstClass . '">';
                if ($hasPostType){
                    $args = array(
                        'post_type' => 'projects',
                        'orderby' => 'date',
                        'posts_per_page' => 3
                    );
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ){
                        while( $query->have_posts() ){
                            $query->the_post();
                            echo '<a class="items">';
                            the_post_thumbnail();
                            echo '</a>';
                        }
                    }
                    if ( isset( $wp_query ) ) {
                        $wp_query->reset_postdata();
                    }
                }else {
                    echo '<img src="' . esc_url($image['url']) . '" alt="' . esc_html($image['alt']) . '"/>';
                }
                echo '</div>';				
                echo '<div class="' . $secondClass . ' wow bounceInUp">';
                echo '<div class="title-box">';
                echo '<h2 class="' . ($titleClass ? $titleClass : '') . '">' . esc_html($title) . '</h2>';
                echo '<h3 class="' . ($subtitleClass ? $subtitleClass : '') . '">' . esc_html($subtitle) . '</h3>';
                echo '</div>';	
                echo '<p class="' . ($contentClass ? $contentClass : '') . '">' . esc_html($content) . '</p>';
                echo '</div>';	
            }
            // Display Categories
            if($args['taxonomy']){
                $argsTax = $args['taxonomy'];
                $taxonomy = $argsTax['taxonomy_name'];
                $hasImage = $argsTax['has_image'];
                $parent_id = $argsTax['parent_id'];
                $args = array(
                    'parent' => $parent_id,
                    'hide_empty' => false
                );
                $terms = get_terms( $taxonomy, $args );
                if ( $terms && !is_wp_error( $terms ) ) {
                echo '<div class="cat-list">';
                    foreach ( $terms as $term ) {
                        $term_link = get_term_link( $term );
                        echo '<a class="items wow bounceInUp" href="' . esc_url( $term_link ) . '">';
                            if ($hasImage == true){
                                $taxImage = get_field('project_categories_thumbnail', $term);
                                echo '<div class="image">';
                                echo '<img src="' . esc_url($taxImage['url'] ) . '" alt="' . esc_attr( $taxImage['alt'] ) . '"/>';
                                echo '</div>';
                                echo '<p>' . strtoupper($term->name) . '</p>';
                            }else{
                                echo strtoupper($term->name);
                            }
                        echo '</a>';
                    }
                echo '</div>';
                }
                if ( isset( $wp_query ) ) {
                    $wp_query->reset_postdata();
                }
            }
            // Display Posts
            if ($hasPost) {
                echo '<div class="homePage-posts">';
                $args = array(
                    'post' => 'posts',
                    'posts_per_page' => 3
                );
                $query = new WP_Query( $args );
                if( $query->have_posts() ){
                    while( $query->have_posts() ){
                        $query->the_post();
                        $excerpt = get_the_excerpt(); 
                        $excerpt = substr( $excerpt, 0, 150 );
                        $result = substr( $excerpt, 0, strrpos( $excerpt, ' ' ) );
                        echo '<div class="items wow bounceInUp">';
                        echo '<a class="img" href="' . esc_url(get_permalink()) . '">';
                        the_post_thumbnail();
                        echo '</a>';
                        echo '<div class="inner">';
                        the_title('<h3 class="archive-title">','</h3>');
                        echo '<p class="archive-excerpt">' . $result . '</p>';
                        echo '<a class="read-more2" href="' . esc_url(get_permalink()) . '">READ MORE</a>';
                        echo '</div>';
                        echo '</div>';
                    }
                }
                echo '</div>';
                if ( isset( $wp_query ) ) {
                    $wp_query->reset_postdata();
                }
            }
        }
        // Biography Page
        elseif ($pageName == 'biography') {
            echo '<div class="' . ($boxClass ? $boxClass : 'inner') . ' wow bounceInUp">';
            echo '<div class="' . ($titleBox ? $titleBox : '') . '">';
            if (isset($title)) {
                echo '<h2 class="' . ($titleClass ? $titleClass : '') . '">' . esc_html($title) . '</h2>';
            }
            if (isset($subtitle)) {
                echo '<h3 class="' . ($subtitleClass ? $subtitleClass : '') . '">' . esc_html($subtitle) . '</h3>';
            }
            echo '</div>';
            if (isset($content)) {
                echo '<p class="' . ($contentClass ? $contentClass : '') . '">' . esc_html($content) . '</p>';
            }
            if ($skills) {
                echo '<div class="skill-box">';
                foreach($skills as $skill){
                    if ($skill['skill_title']) {
                        echo '<div class="skills">';
                        echo '<h4>' . esc_html($skill['skill_title']) . '</h4>';
                        echo '<i>' . esc_html($skill['skill_percent']) . '%</i>';
                        echo '<span percent="' . esc_html($skill['skill_percent']) . '"><span></span></span>';
                        echo '</div>';
                    }
                }
                echo '</div>';
            }
            if ($degrees) {
                echo '<div class="degrees-box">';
                $count = 0;
                foreach($degrees as $degree){
                    if ($degree['title']) {
                        $count++;
                        echo '<div class="degrees ' . ($count % 2 == 0 ? 'even' : '') . '">';
                        echo '<div class="inner">';
                        echo '<h3>' . esc_html($degree['title']) . '</h3>';
                        echo '<i class="badge">' . esc_html($degree['date']) . '</i>';
                        echo '</div>';
                        echo '<div>';
                        echo '<span>' . esc_html($degree['line_1']) . '</span>';
                        echo '<span>' . esc_html($degree['line_2']) . '</span>';
                        echo '</div>';
                        echo '</div>';
                    }
                }
                echo '</div>';
            }
            if ($experiences) {
                echo '<div class="experiences-box">';
                foreach($experiences as $experience){
                    if ($experience['title']) {
                        $count++;
                        echo '<div class="experiences">';
                        echo '<div class="title-box">';
                        echo '<h3 class="title2-style">' . esc_html($experience['title']) . '</h3>';
                        echo '<h4 class="subtitle-style">' . esc_html($experience['subtitle']) . '</h4>';
                        echo '</div>';
                        echo '<div class="inner">';
                        echo $experience['content'];
                        echo '</div>';
                        echo '</div>';
                    }
                }
                echo '</div>';
            }
            if ($socialMedia) {
                echo '<div class="contact-me">';
                echo '<div class="socialMedia-box">';
                echo '<h3 class="subtitle">' . esc_html($subtitle1) . '</h3>';
                ?>
                <ul>
					<li>
						<a href="<?php echo esc_url($socialMedia['facebook']); ?>">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url($socialMedia['linkedin']); ?>">
							<i class="fab fa-linkedin-in"></i>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url($socialMedia['instagram']); ?>">
							<i class="fab fa-instagram"></i>						
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url($socialMedia['pinterest']); ?>">
							<i class="fab fa-pinterest-p"></i>						
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url($socialMedia['behance']); ?>">
							<i class="fab fa-behance"></i>						
						</a>
					</li>
				</ul>
                <?php
                echo '</div>';
                echo '<h3 class="subtitle">' . esc_html($subtitle2) . '</h3>';
                echo do_shortcode($contactForm);
                echo '</div>';
            }
            echo '</div>';
        }
        // Display Sliders
        if ($sliders){
            echo '<div class="' . ($sliderClass ? $sliderClass : '') . '">';
            foreach($sliders as $slider){
                if($slider){
                    echo '<div class="items">';
                    echo '<img src="' . esc_url($slider['url']) . '" alt="' . esc_html($slider['alt']) . '">';
                    echo '</div>';
                }
            }
            echo '</div>';
        }
    }
}
?>