jQuery(document).ready(function(){
    new WOW().init();
});

// Slick Sliders
jQuery(document).ready(function(){
    // Home Page Header Slider
    jQuery('.homePage-header-slider').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        autoplay: true,
        dots: false,
        arrows : true,
        cssEase: 'linear',
        rtl: false,
    });

    // Home Page Section-2 Slider
    jQuery('.homePage-sec2-slider').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        autoplay: true,
        dots: false,
        arrows : true,
        cssEase: 'linear',
        rtl: false,
    });

    // Home Page Section-4 Slider
    jQuery('.homePage-sec4-slider').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        autoplay: true,
        dots: false,
        arrows : true,
        cssEase: 'linear',
        rtl: false,
    });

    // Home Page Section-6 Slider
    jQuery('.homePage-sec6-slider').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        // autoplay: true,
        dots: false,
        arrows : true,
        cssEase: 'linear',
        rtl: false,
        fade: true,
    });

    // Biography Page Header Slider
    jQuery('.biographyPage-header-slider').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        // autoplay: true,
        dots: false,
        arrows : true,
        cssEase: 'linear',
        rtl: false,
        fade: true,
    });

    // Archive Categories Slider
    jQuery('.archive-cat-slider').each(function(){
        jQuery(this).slick({
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            adaptiveHeight: true,
            // autoplay: true,
            dots: false,
            arrows : true,
            cssEase: 'linear',
            rtl: false,
            fade: true,
        })
    });

    // Single Categories Slider
    jQuery('.single-cat-posts-slider').each(function(){
        jQuery(this).slick({
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            adaptiveHeight: true,
            // autoplay: true,
            dots: false,
            arrows : true,
            cssEase: 'linear',
            rtl: false,
        })
    });
});

// drop down mobile menu ------------------------------------------------
jQuery(document).ready(function(){
    jQuery(".menu-icon").click(function() {
        jQuery('.menu-list').animate({left: '0'});
        jQuery('.menu-icon').hide();
        jQuery('.menu-icon-close').show();
    })
    jQuery(".menu-icon-close").click(function() {
        jQuery('.menu-list').animate({left: '-100%'});
        jQuery('.menu-icon-close').hide();
        jQuery('.menu-icon').show();
    })
});

// Scroll To Top Button ------------------------------------------------
jQuery(".scroll-to-top > a").on("click", function() {
    jQuery("html").animate({ scrollTop: 0 }, "slow");
});

// Progress bar (Skills) ------------------------------------------------
jQuery(document).ready(function(){
    jQuery('.skills').children('span').each(function(){
        var percent = jQuery(this).attr("percent");
        console.log(percent);
        jQuery(this).find('span').animate({'width' : percent + '%'}, 2500);
    })
});