<?php
/**
 * Home page template file
 * Template Name: Home Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package byte
 */

get_header();
?>

	<main id="primary" class="home-page site-main">
        <section class="section-1">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array(
				'page_name' => 'home',
				'ACF_Name' => 'home_page_header_slider',
				'slider' => true,
				'classes'  => array(
					'slider' => 'homePage-header-slider',
				)
			));
			?>
		</section><!-- .section-1 -->

		<section class="sections section-2">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_graphic_design',
				'title' => true,
				'content' => true,
				'link' => true,
				'slider' => true,
				'taxonomy' => array(
					'taxonomy_name' => 'projects_categories',
					'parent_id' => 3,
					'has_image' => false,
				),
				'classes'  => array(
					'title' => 'title-style',
					'content' => 'content-style',
					'button' => 'read-more',
					'slider' => 'homePage-sec2-slider',
				)
			));
			?>
		</section><!-- .section-2 -->

		<section class="section-3">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_photography',
				'title' => true,
				'content' => true,
				'link' => true,
				'taxonomy' => array(
					'taxonomy_name' => 'projects_categories',
					'parent_id' => 14,
					'has_image' => true,
				),
				'classes'  => array(
					'title' => 'title-style',
					'content' => 'content-style',
					'button' => 'read-more',
				)
			));
			?>
		</section><!-- .section-3 -->
		
		<section class="sections section-4">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_caligraphy',
				'title' => true,
				'subtitle' => true,
				'content' => true,
				'image' => true,
				'classes'  => array(
					'title' => 'title2-style',
					'subtitle' => 'subtitle-style',
					'content' => 'content2-style',
				)
			));
			?>
		</section><!-- .section-4 -->

		<section class="sections section-5">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_my_brand',
				'title' => true,
				'content' => true,
				'link' => true,
				'slider' => true,
				'classes'  => array(
					'title' => 'title-style',
					'content' => 'content-style',
					'button' => 'read-more',
					'slider' => 'homePage-sec4-slider',
				)
			));
			?>
		</section><!-- .section-5 -->

		<section class="sections section-6">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_recent_projects',
				'title' => true,
				'subtitle' => true,
				'content' => true,
				'has_postType' => true,
				'classes'  => array(
					'title' => 'title2-style',
					'subtitle' => 'subtitle-style',
					'content' => 'content2-style',
				)
			));
			?>
		</section><!-- .section-6 -->

		<section class="sections section-7">
		<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_researches',
				'title' => true,
				'content' => true,
				'link' => true,
				'has_post' => true,
				'classes'  => array(
					'title' => 'title-style',
					'content' => 'content-style',
					'button' => 'read-more',
				)
			));
			?>
		</section><!-- .section-7 -->

		<section class="sections section-8">
			<?php 
			get_template_part( 'template-parts/content-page-builder', null, array( 
				'page_name' => 'home',
				'ACF_Name' => 'home_page_about_me',
				'title' => true,
				'subtitle' => true,
				'content' => true,
				'image' => true,
				'classes'  => array(
					'title' => 'title2-style',
					'subtitle' => 'subtitle-style',
					'content' => 'content2-style',
				)
			));
			?>
		</section><!-- .section-8 -->
	</main><!-- .home-page -->

<?php
get_footer();
