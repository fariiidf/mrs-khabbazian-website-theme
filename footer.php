<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package byte
 */

?>

<footer id="colophon" class="site-footer">
    <div class="footerNav footer-menu">
        <nav class="main-navigation left">
            <?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer-menu-left',
						'menu_id'        => 'footer-menu-left',
						'menu_class'        => 'uppercase'
					)
				);
				?>
        </nav>
        <div class="site-branding logo">
            <?php the_custom_logo(); ?>
        </div>
        <nav class="main-navigation right">
            <?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer-menu-right',
						'menu_id'        => 'footer-menu-right',
						'menu_class'        => 'uppercase'
					)
				);
				?>
        </nav>
    </div>
    <div class="how-to-contact">
        <i>HOW TO CONTACT ME</i>
        <hr>
    </div>
    <div class="footer-contact">
        <div class="send-message">
            <a href="<?php echo get_page_link( get_page_by_title( 'biography' )->ID ); ?>#contact-me">SEND MESSAGE</a>
        </div>
        <i>OR</i>
        <div class="footer-social-media">
            <ul>
                <li>
                    <a href="<?=get_field("biography_page_section_6_social_media_links_facebook", 15)?>">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="<?=get_field("biography_page_section_6_social_media_links_linkedin", 15)?>">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
                <li>
                    <a href="<?=get_field("biography_page_section_6_social_media_links_instagram", 15)?>">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="<?=get_field("biography_page_section_6_social_media_links_pinterest", 15)?>">
                        <i class="fab fa-pinterest-p"></i>
                    </a>
                </li>
                <li>
                    <a href="<?=get_field("biography_page_section_6_social_media_links_behance", 15)?>">
                        <i class="fab fa-behance"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
</body>

</html>